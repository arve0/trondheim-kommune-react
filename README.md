# React-app template

## Filstruktur

Filstrukturen er basert på artikkelen [Three Rules For Structuring (Redux) Applications](https://jaysoo.ca/2016/02/28/organizing-redux-application/).

De tre reglene er:

1. Organize by features
2. Create strict module boundaries
3. Avoid circular dependencies

### Mapper

- `components` - Dumme komponenter som brukes på _flere_ sider _uten_ koblet tilstand.
- `containers` - Smarte komponenter som brukes på _flere_ sider _med_ koblet tilstand.
- `pages` - Sidene til appen. Hver side er i sin egen mappe.
- `services` - Moduler som ikke er relatert til UI. Eksempelvis http-endepunkt, hjelpe-funksjoner, osv.

### QA

Q: Komponent-mappen er uoversiktelig, kan relaterte komponenter grupperes?

A: Ja. Grupper de i en egen mappe, slik som `components/buttons`.

---

Q: Hvor går styling?

A:

## Utvikling
Denne appen er et skall fra create-react-app, se [DEVELOPMENT.md](DEVELOPMENT.md) for mer informasjon.