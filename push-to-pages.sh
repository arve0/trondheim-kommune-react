#!/bin/sh

PUBLIC_FOLDER=react
COMMIT=$(git rev-parse HEAD)

git clone git@bitbucket.org:arve0/arve0.bitbucket.io.git pages

rm -fr pages/$PUBLIC_FOLDER
mv build pages/$PUBLIC_FOLDER

git config --global user.email "pipeline@bitbucket.org"
git config --global user.name "Bitbucket Pipeline"

git -C pages add --all
git -C pages commit -m "https://bitbucket.org/arve0/trondheim-kommune-react/commits/$COMMIT"

git -C pages push origin master