import React from 'react';
import { Provider } from 'react-redux';
import { render } from 'react-testing-library';

import App from './App';
import store from './store'

jest.mock('services/CourseService.js')

it('renders without crashing', () => {
  render(
    <Provider store={store}><App /></Provider>
  );
});
