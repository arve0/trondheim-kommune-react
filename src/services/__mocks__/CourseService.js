let courses = [{
    _id: '1',
    name: 'Fysikk 1',
    description: '2ST 18/19'
}]

let currentId = 1

export function fetchCourses () {
    return Promise.resolve(courses)
}

export function add (course) {
    currentId += 1
    course._id = currentId.toString()
    courses.push(course)
    return Promise.resolve(course)
}

export function save (course) {
    let index = courses.findIndex(k => k._id === course._id)
    courses[index] = course

    return Promise.resolve(course)
}

export function delete_ (course) {
    courses = courses.filter(k => k._id !== course._id)
    return Promise.resolve()
}