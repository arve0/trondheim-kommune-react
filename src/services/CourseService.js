//let URL = 'http://localhost:8080/klasse'
let URL = 'https://rest.dokku.seljebu.no/klasse'

export function fetchCourses () {
    return fetch(URL).then(r => r.json())
}

export function add (course) {
    return fetch(URL, {
        method: 'POST',
        mode: 'cors',
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(course),
    }).then(r => r.json())
}

export function save (course) {
    return fetch(URL + `/${course._id}`, {
        method: 'PUT',
        mode: 'cors',
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(course),
    }).then(_ => course)
}

export function delete_ (id) {
    return fetch(URL + `/${id}`, {
        method: 'DELETE',
        mode: 'cors',
    })
}