import React, { Component } from 'react';
import { BrowserRouter as Router, Link, Route } from 'react-router-dom';
import './App.css';
import Courses from 'pages/courses';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <header className="App-header">
            <h1 className="App-title">Trondheim kommune React-template</h1>
            <Link to="/">Hjem</Link>
            <Link to="/kurs">Kurs</Link>
            <Link to="/om">Om</Link>
          </header>
          <div id="content">
            <Route exact={true} path="/" render={() => <h1>Velkommen</h1>} />
            <Route path="/kurs" component={Courses} />
            <Route path="/om" component={About} />
          </div>
        </div>
      </Router>
    );
  }
}

const About = () => <div>Om denne siden</div>

export default App;
