import React from 'react';
import PropTypes from 'prop-types';

const CourseEdit = ({ course }) =>
  <div key={course._id}>  {/* make sure we update when getting new course */}
    <h1 className="red">{course.name}</h1>
    <table>
      <tbody>
        <tr>
          <th>ID</th>
          <td>{course._id}</td>
        </tr><tr>
          <th><label htmlFor="course-name">Navn</label></th>
          <td><input id="course-name" defaultValue={course.name} onChange={ e => course.name = e.target.value } /></td>
        </tr><tr>
          <th><label htmlFor="course-desc">Beskrivelse</label></th>
          <td><input id="course-desc" defaultValue={course.description} onChange={ e => course.description = e.target.value } /></td>
        </tr>
      </tbody>
    </table>
  </div>

CourseEdit.propTypes = {
  course: PropTypes.object.isRequired,
}

export default CourseEdit;