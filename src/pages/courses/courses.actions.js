import { fetchCourses, save, delete_ } from 'services/CourseService'
import { add } from '../../services/CourseService';

export const SET_COURSES = 'SET_COURSES'
export const COURSE_SAVED = 'COURSE_SAVED'
export const ADD_COURSE = 'ADD_COURSE'
export const DELETE_COURSE = 'DELETE_COURSE'

const setCourses = (courses) => ({
    type: SET_COURSES,
    courses
})

export const getCourses = () => (dispatch) => {
    // TODO: expand to dispatch loading action
    return fetchCourses().then(courses => dispatch(setCourses(courses)))
}

const courseSaved = (course) => ({
    type: COURSE_SAVED,
    course
})

const addCourse = (course) => ({
    type: ADD_COURSE,
    course
})

export const saveCourse = (course) => (dispatch) => {
    if (course._id) {
        return save(course).then(c => dispatch(courseSaved(c)))
    }
    return add(course).then(c => dispatch(addCourse(c)))
}

const courseDeleted = (id) => ({
    type: DELETE_COURSE,
    id
})

export const deleteCourse = (id) => (dispatch) => {
    return delete_(id).then(_ => dispatch(courseDeleted(id)))
}
