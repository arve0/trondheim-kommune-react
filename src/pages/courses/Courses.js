import React, { Component } from 'react';
import { connect } from 'react-redux'
import CourseEdit from './CourseEdit';
import { getCourses, saveCourse, deleteCourse } from './courses.actions'

let liStyle = {
    cursor: 'pointer'
}

class Courses extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: null,
        };

        this.props.dispatch(getCourses())

        this.handleSelect.bind(this);
        this.handleNew = this.handleNew.bind(this);
        this.handleSave = this.handleSave.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleSelect(course) {
        this.setState({ selected: course });
    }

    handleNew() {
        this.handleSelect({
            _id: '', name: '', description: ''
        })
    }

    handleSave() {
        this.props.dispatch(saveCourse(this.state.selected)).then(_ => {
            this.setState({ selected: null })
        })
    }

    handleDelete () {
        this.props.dispatch(deleteCourse(this.state.selected._id)).then(_ => {
            this.setState({ selected: null })
        })
    }

    render() {
        return <div>
            <ul>
            {this.props.courses.map(k =>
                <li key={k._id} onClick={() => this.handleSelect(k)} style={liStyle}>{k.name}</li>
            )}
            </ul>

            <button onClick={this.handleNew}>Legg til ny</button>

            {this.state.selected &&
                <div>
                    <CourseEdit course={this.state.selected} />
                    <button onClick={this.handleSave}>Lagre</button>
                    {isSavedInDatabase(this.state.selected)
                        && <button onClick={this.handleDelete}>Slett</button>
                    }
                </div>
            }
        </div>
    }
}

function isSavedInDatabase (course) {
    return course._id
}

const mapStateToProps = state => ({
    courses: state.courses
})

export default connect(mapStateToProps)(Courses);