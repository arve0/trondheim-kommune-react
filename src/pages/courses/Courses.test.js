import React from 'react';
import { Provider } from 'react-redux';
import { render, fireEvent, wait, waitForElement } from 'react-testing-library';
import { default as UnconnectedCourses } from './Courses';
import store from 'store'

const Courses = () => <Provider store={store}><UnconnectedCourses /></Provider>

jest.mock('services/CourseService')

it('renders without crashing', (done) => {
    const courses = render(<Courses />);
    process.nextTick(() => {
        expect(courses.queryByText('Fysikk 1')).toBeInTheDocument()
        done();
    });
});

it('can add items', async () => {
    const { getByText, getByLabelText, queryByText } = render(<Courses />);

    const newItem = getByText('Legg til ny')
    newItem.click()

    expect(queryByText('Lagre')).toBeInTheDocument()

    const nameInput = getByLabelText('Navn')
    fireEvent.change(nameInput, { target: { value: 'aaaa' } })

    const descInput = getByLabelText('Beskrivelse')
    fireEvent.change(descInput, { target: { value: 'bbbb' } })

    getByText('Lagre').click()

    await waitForElement(() => getByText('aaaa', { selector: 'li' }), { timeout: 500 })
});

it('can delete items', async () => {
    const { getByText, queryByText } = render(<Courses />);

    const item = await waitForElement(() => getByText('aaaa'))
    item.click()

    const deleteButton = getByText('Slett')
    deleteButton.click()

    await wait(() => {
        if (queryByText('aaaa', { selector: 'li' }) !== null) {
            throw new Error()
        }
    }, { timeout: 500 })
});

it('can edit items', async () => {
    const { getByText, getByLabelText } = render(<Courses />);

    const item = await waitForElement(() => getByText('Fysikk 1'))
    item.click()

    const nameInput = getByLabelText('Navn')
    fireEvent.change(nameInput, { target: { value: 'Fysikk 2' } })

    getByText('Lagre').click()

    await waitForElement(() => getByText('Fysikk 2', { selector: 'li' }), { timeout: 500 })
});

