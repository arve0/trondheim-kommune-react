import { SET_COURSES, ADD_COURSE, COURSE_SAVED, DELETE_COURSE } from './courses.actions'

export default function (state = [], action) {
    switch (action.type) {
        case SET_COURSES:
            return action.courses
        case ADD_COURSE:
            return [ ...state, action.course ]
        case DELETE_COURSE:
            return state.filter(course => course._id !== action.id)
        case COURSE_SAVED:
            let courseIndex = state.findIndex(course => course._id === action.course._id)
            return [ ...state.slice(0, courseIndex), action.course, ...state.slice(courseIndex + 1) ]
        default:
            return state
    }
}