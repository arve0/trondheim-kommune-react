import { createStore, applyMiddleware, combineReducers } from 'redux'
import thunk from 'redux-thunk'

import { reducer as courses } from "pages/courses";

const rootReducer = combineReducers({
    courses: courses
})

export default createStore(rootReducer, applyMiddleware(thunk))
